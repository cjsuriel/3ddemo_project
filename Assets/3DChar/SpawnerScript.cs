using System.Collections.Generic;
using UnityEngine;


public class SpawnerScript : MonoBehaviour
{
  public GameObject prototype = null;
  public int max_spawned_objects = 1;
  public float time_between_spawns = 2f;

  private readonly List<GameObject> spawned = new List<GameObject>();
  private float timer = 0f;

  private void Update()
  {
    if ( timer > 0f )
    {
      timer -= Time.deltaTime;
      return;
    }
    if ( Spawn() )
      timer = time_between_spawns;
  }

  private bool Spawn()
  {
    if ( !prototype )
      return false;
    spawned.RemoveAll( x => x == null );
    if ( spawned.Count >= max_spawned_objects )
      return false;
    GameObject go = Instantiate( prototype, transform.position, transform.rotation );
    spawned.Add( go );
    return true;
  }
}
